<?php

namespace App\Http\Controllers;

use App\Heroe;
use Illuminate\Http\Request;

class AngularHeroesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $heroes = request()->user()->heroes;
 
        return response()->json([
            'heroes' => $heroes,
        ], 200);
    }
}
