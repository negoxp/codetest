<?php

namespace App\Http\Controllers;

use App\Heroe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HeroeController extends Controller
{
    /**
     * Middleware security in the controller
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('heroe/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('heroe/new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate that all the values exist
        $this->validate($request, [
            'name'        => 'required|max:255',
            'hit_points'       => 'required|max:255',
            'attack'       => 'required|max:255',
            'special_ability'       => 'required|max:255',
            'team'       => 'required'
        ]);

        //Create a new Heroe
        //TODO: Avatar is generated in the model. attach image future
        $heroe = new Heroe;
        $heroe->user_id = Auth::user()->id;
        $heroe->name = request('name');
        $heroe->hit_points = request('hit_points');
        $heroe->attack = request('attack');
        $heroe->special_ability = request('special_ability');
        $heroe->team = request('team');
        $heroe->save(); 

        return redirect()->action('HeroeController@index')->with('message','Sucess - You create a new Heroe.');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Heroe  $heroe
     * @return \Illuminate\Http\Response
     */
    public function show(Heroe $heroe)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Heroe  $heroe
     * @return \Illuminate\Http\Response
     */
    public function edit(Heroe $heroe)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Heroe  $heroe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Heroe $heroe)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Heroe  $heroe
     * @return \Illuminate\Http\Response
     */
    public function destroy(Heroe $heroe)
    {
        //
    }

}
