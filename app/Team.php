<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    /**
    * Relationship User Has many heroes 
    * 
    */

    public function heroes()
    {
        return $this->hasMany(Heroe::class, 'user_id');
    }


    public function save(array $options = [])
   	{
		$this->team_hit_points=0;
		parent::save();
   	}
}
