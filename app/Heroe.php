<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Heroe extends Model
{
    //

	protected $fillable = ['user_id', 'name'];

	/**
    * Relationship Heroes belongs to a User
    * 
    */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id'); 
    }


    public function save(array $options = [])
   	{
   		/* Create the avatar Image
   		*  More information https://jdenticon.com/
   		*  https://jdenticon.com/php-get-started.html
		*/
		$icon = new \Jdenticon\Identicon();
		$icon->setValue($this->name);
		$icon->setSize(100);

		//Same the avatar image in the storage
		$file_data = $icon->getImageDataUri('png'); 
		$file_name = 'image_'.time().'.png'; //generating unique file name; 
		@list(, $file_data) = explode(',', $file_data); 
		if($file_data!=""){ // storing image in storage/app/public Folder 
			Storage::disk('public')->put($file_name,base64_decode($file_data)); 
		} 

		//Add the ulr to the avatar before to save
		$this->avatar=Storage::url($file_name);
		$this->team_id=0;

		parent::save();

   }
}
