@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Create Heroe
                </div>
                <div class="card-body">
                {{ Form::open(array('url' => 'myheroes')) }}

                    <div class="form-group">
                        {{ Form::label('name', 'Name') }} 
                        {{ Form::text('name', null, array('class' => 'form-control', 'required' => 'required' )) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('hit_points', 'Hit Points (Number)') }}
                        {{ Form::number('hit_points', null, array('class' => 'form-control', 'required' => 'required')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('attack', 'Attack') }}
                        {{ Form::text('attack', null, array('class' => 'form-control', 'required' => 'required')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('special_ability', 'Special Ability') }}
                        {{ Form::text('special_ability', null, array('class' => 'form-control', 'required' => 'required')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('team', 'Special Ability') }}
                        {{ Form::select('team', array('light' => 'Light', 'dark' => 'Dark'), 'light' , array('class' => 'form-control', 'required' => 'required')) }}
                    </div>

                    {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}

                {{ Form::close() }}
                </div>



            </div>
        </div>
    </div>
</div>
@endsection