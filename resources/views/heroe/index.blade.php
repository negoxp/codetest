@extends('layouts.app')

@section('content')
<div class="container" ng-controller="HeroesController">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    My Heroes
                    <a href="{{url('myheroes/create')}}" class="btn btn-info float-right"> Create Heroe </a>
                </div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row" ng-if="heroes.length > 0">
                        <div class="col-md-3" ng-repeat="(index, heroe) in heroes">
                            <div class="jumbotron">
                                <h4>@{{ heroe.name }}</h4>
                                <img src="@{{heroe.avatar}}" />
                                </br>
                                Hit Points: <strong>@{{ heroe.hit_points }}</strong></br>
                                Attack: <strong>@{{ heroe.attack }}</strong></br>
                                Special Ability: <strong>@{{ heroe.special_ability }}</strong></br>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
