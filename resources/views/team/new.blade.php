@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Create Team
                </div>
                <div class="card-body">
                {{ Form::open(array('url' => 'team')) }}

                    <div class="form-group">
                        {{ Form::label('name', 'Name') }} 
                        {{ Form::text('name', null, array('class' => 'form-control', 'required' => 'required' )) }}
                    </div>

                    {{ Form::submit('Create', array('class' => 'btn btn-primary')) }}

                {{ Form::close() }}
                </div>



            </div>
        </div>
    </div>
</div>
@endsection