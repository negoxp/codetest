
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import 'angular';


angular.module('AngularApp', []).controller('HeroesController', function ($scope,$http) {
    $scope.tasks = [];
     
    // List Heroes
    $scope.loadHeroes = function () {
        $http.get('/angularHeroes')
            .then(function success(e) {
                $scope.heroes = e.data.heroes;
            });
    };
    $scope.loadHeroes();

    $scope.errors = [];


});